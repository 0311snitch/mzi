from skimage.util import view_as_blocks
from utils import dct, abs_diff_coeffs, to_bits, from_bits
from data import N
from skimage import io
import sys

def get_bit(block):
    transform = dct(dct(block, axis=0), axis=1)
    return 0 if abs_diff_coeffs(transform) > 0 else 1


def get_message(img, length, c):
    blocks = view_as_blocks(img[:, :, c], block_shape=(N, N))
    s = blocks.shape[1]
    return [get_bit(blocks[index // s, index % s]) for index in range(length)]


if __name__ == "__main__":
    """
    Color ids:
        RED - 0;
        YELLOW - 1;
        BLUE - 2;
    """
    if len(sys.argv) < 3:
      sys.exit(0)
    color = int(sys.argv[2])
    if color > 2:
      print("ERROR: color must be 0, 1, 2.")
      sys.exit(1)
    img = io.imread(sys.argv[1])
    length = int(sys.argv[3]) * 8 if len(sys.argv) > 3 else len(img[:,:,color])
    bit_message = get_message(img, length, color)
    print(f"Decrypted message - {from_bits(bit_message)}")