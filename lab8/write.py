
from skimage.util import view_as_blocks
from utils import dct, idct, to_bits, valid_coeffs, change_coeffs, dtb
from read import retrieve_bit
from skimage import io
from data import *
import sys

def encrypt_bit(block, bit):
    patch = block.copy()
    cfs = dct(dct(patch, axis=0, norm='ortho'), axis=1, norm='ortho')
    while not valid_coeffs(cfs, bit, P) or (bit != retrieve_bit(patch)):
        cfs = change_coeffs(cfs, bit)
        patch = dtb(idct(idct(cfs, axis=0, norm='ortho'), axis=1, norm='ortho'))
    return patch


def encrypt(orig, msg, color):
    changed = orig.copy()
    print(color)
    cchannel = changed[:, :, color]
    if len(msg) > len(cchannel):
      print('Your message is too long for this picture')
      sys.exit(0)
    blocks = view_as_blocks(cchannel, block_shape=(N, N))
    h = blocks.shape[1]
    for index, bit in enumerate(msg):
        i = index // h
        j = index % h
        block = blocks[i, j]
        cchannel[i * N: (i + 1) * N, j * N: (j + 1) * N] = encrypt_bit(block, bit)
    changed[:, :, color] = cchannel
    return changed


if __name__ == "__main__":
    if len(sys.argv) < 2:
      sys.exit(0)
    message = to_bits(input("Enter to message to encrypt: "))
    color = 0
    if len(sys.argv) == 3:
      color = int(sys.argv[2])
    if color > 2:
      print("ERROR: color must be 0, 1, 2.")
      sys.exit(1)
    original_jpg = io.imread(sys.argv[1])
    img = encrypt(original_jpg, message, color)
    io.imsave('encrypted.jpeg', img, quality=100)
    print("Saved to encrypted.jpeg")
