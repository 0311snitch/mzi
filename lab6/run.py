from gost_2 import EllipticCurve, sign, verify, prv_unmarshal, generate_public_key
from data import CURVES
from gost_1 import GOST341112
import os


KEY_LENGTH = 128

def generate_keys(curve):
    privkey = prv_unmarshal(os.urandom(KEY_LENGTH))
    pubkey = generate_public_key(curve, privkey)

    return pubkey, privkey

message = "123456"

hashed_message = GOST341112(data=bytes(message, "utf-8")).digest()

E = EllipticCurve(*(CURVES[0]))
public_key, private_key = generate_keys(E)

print('Public key:', public_key)
print('Private key:', private_key)

signature = sign(E, private_key, hashed_message)
print('Signature', signature)

print('Success: ', verify(E, public_key, hashed_message, signature))