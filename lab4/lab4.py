import random
import sys
import math

FROM = 100
TO = 2000

def isPrime(n):
    if n % 2 == 0:
        return n == 2
    d = 3
    while d * d <= n and n % d != 0:
       d += 2
    return d * d > n

def module_exp(base, power, m):
    return pow(base, power) % m


def get_p():
    rand = 0
    while not isPrime(rand):
        rand = random.randint(FROM, TO)
    return rand


def get_prime_factors(num):
    prime_factors = []
    while not num % 2:
        prime_factors.append(2)
        num /= 2

    end = int(math.sqrt(num)) + 1
    for i in range(3, end, 2):
        while not num % i:
            prime_factors.append(i)
            num /= i
    return prime_factors


def get_root(p, prime_factors):
    g = random.randint(2, p - 1)
    for i in range(len(prime_factors)):
        if module_exp(g, int((p - 1) / prime_factors[i]), p) == 1:
            return get_root(p, prime_factors)
    return g


def egcd(a, b):
    result = a if b == 0 else egcd(b, a % b)
    return result


def get_k(p):
    while True:
        k = random.randint(1, p - 1)
        if egcd(k, p - 1) == 1:
            return k


def generate_keys():
    p = get_p()
    g = get_root(p, get_prime_factors(p-1))
    x = random.randint(2, p - 1)
    y = module_exp(g, x, p)
    print(
        f"""
        PUBLIC KEYS:
            p = {p}
            g = {g}
            y = {y}
        """
    )
    print(
        f"""
        SECRET KEY
            p = {p} 
            x = {x}
        """
    )
    return (p, g, y), x


def encrypt_char(ch, p, g, y):
    k = get_k(p)
    char_code = ord(ch)
    a = module_exp(g, k, p)
    b = y ** k * char_code % p
    return a, b

def encypt(message, p, g, y):
    encrypted_message = []
    for m in message:
        a, b = encrypt_char(m, p, g, y)
        encrypted_message.append(a)
        encrypted_message.append(b)
    return encrypted_message

def decrypt_char(a, b, p, x):
    return chr((b * (a ** (p - 1-x))) % p)

def decrypt(int_message, p, x):
    decrypted_chars = []
    for i in range(0, len(int_message) - 1, 2):
        a = int_message[i]
        b = int_message[i + 1]
        m = decrypt_char(a, b, p, x)
        decrypted_chars.append(m)
    return decrypted_chars


def demo():
    (p, g, y), private_x = generate_keys()
    message = input('Enter text to encrypt: ')
    message_chars = []
    for m in message:
        message_chars.append(ord(m))
    print(f"Message - {message_chars}")
    encrypted = encypt(message, p, g, y)
    print(f"Encrypted - {encrypted}")
    decrypted = decrypt(encrypted, p, private_x)
    print(f"Decrypted - {''.join(decrypted)}")


if __name__ == "__main__":
    demo()