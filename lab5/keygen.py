from random import randint
from hmac import write

key = bytearray([randint(0, 255) for _ in range(64)])
write('private_key.bin', key)
print("You can find a key in 'private_key.bin'")