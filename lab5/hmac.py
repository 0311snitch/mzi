from hashlib import md5
import sys

def write(filename, key):
     with open(filename, 'wb') as f:
          f.write(key)


def read(filename):
    with open(filename, "rb") as f: 
      return f.read()

def hmac(key, msg):
    hash = md5()
    bz = hash.block_size
    if len(key) > bz:
        key = hash.update(key).digest 
    if len(key) < bz:
        key += bytearray([0]*(bz))
    ipad = [0x36] * bz
    opad = [0x5c] * bz
    ikeypad = [ipad[i] ^ key[i] for i in range(bz)]
    okeypad = [opad[i] ^ key[i] for i in range(bz)]
    h1 = md5()
    h1.update(bytearray(ikeypad) + msg) 
    h2 = md5()
    h2.update(bytearray(okeypad) + h1.digest())
    return h2.digest()


if __name__ == "__main__":
    text_file = sys.argv[1]
    key_file = sys.argv[2]
    file_hash_file = None
    file_hash_compare = 0
    if len(sys.argv) == 4:
        file_hash_file = sys.argv[3]
        file_hash_compare = read(file_hash_file)
    key = bytearray(read(key_file))    
    file_hash = 0
    with open(text_file, "r", encoding="utf-8") as f:
        message = f.read()
        file_hash = hmac(bytearray(message, "utf-8"), key)
    if file_hash_file is not None:
        print("The file isn't changed" if file_hash == file_hash_compare else "Hashes aren't equal. The file has been modified")
    else:
        write('hash.bin', file_hash)
        print("You can find a new hash in 'hash.bin'")