import random
from data import *

def binaryze(b, required_length=0):
    res = []
    while b:
        res.append(b & 1)
        b >>= 1
    for _ in range(required_length - len(res)):
        res.append(0)
    return reversed(res)


def debinaryze(seq):
    b = 0
    for bit in seq:
        b = (b << 1) + bit
    return b


def feistel(d, key):
    data = [d[i-1] for i in E]
    data = [data[i] ^ key[i] for i in range(len(data))]
    result = []
    for bi in range(8):
        bi_start = bi * 6
        a = data[bi_start] * 2 + data[bi_start+5]
        b = 0
        sh = 8
        for i in range(bi_start+1, bi_start+5):
            b += sh * data[i]
            sh >>= 1
        result += binaryze(S[bi][a][b], 4)
    return [result[i-1] for i in P]


def generate_keys(key):
    kb = [[] for i in range(8)]
    for index, bit in enumerate(key):
        kb[index // 7].append(bit)
    for byte in kb:
        ch = 0
        for bit in byte:
            ch = ch ^ bit
        byte.append(ch ^ 1)
    extended_key = []
    for byte in kb:
        extended_key += byte
    c0 = [extended_key[pos-1] for pos in C0]
    d0 = [extended_key[pos-1] for pos in D0]
    res = []
    ci, di = c0, d0
    for shift in SH:
        ci = ci[shift:] + ci[:shift]
        di = di[shift:] + di[:shift]
        full_key = ci + di
        res.append([full_key[pos-1] for pos in K])
    return res


def encrypt(message, key):
    mad = bytearray([len(message) // 256, len(message) % 256])
    while (len(mad) + len(message)) % 8:
        mad.append(random.randrange(0,256,1))
    message = mad + message
    keys = generate_keys(binaryze(key, 56))
    result = bytearray()
    for bs in range(0, len(message), 8):
        d = []
        for i in range(bs, bs+8):
            d += binaryze(message[i], 8)
        d_ip = [d[i-1] for i in IP] 
        previous_left = d_ip[:32]
        previous_right = d_ip[32:]
        for step in range(16):
            next_left = previous_right
            f = feistel(previous_right, keys[step])
            next_right = [f[i] ^ previous_left[i] for i in range(len(f))]
            previous_left = next_left
            previous_right = next_right
        new_d = previous_left + previous_right
        d_res = [new_d[pos-1] for pos in IP_n]
        for i in range(0, 64, 8):
            result.append(debinaryze(d_res[i:i+8]))
    return result


def decrypt(message, key):
    keys = generate_keys(binaryze(key, 56))
    result = bytearray()
    for bs in range(0, len(message), 8):
        d = []
        for i in range(bs, bs+8):
            d.extend(binaryze(message[i], 8))
        d_ip = [d[i-1] for i in IP]
        previous_left = d_ip[:32]
        previous_right = d_ip[32:]
        for step in range(16, 0, -1):
            next_right = previous_left
            res = feistel(previous_left, keys[step-1])
            next_left = [res[i] ^ previous_right[i] for i in range(len(res))]
            previous_left = next_left
            previous_right = next_right
        new_d = previous_left + previous_right
        d_res = [new_d[pos-1] for pos in IP_n]
        for i in range(0, 64, 8):
            result.append(debinaryze(d_res[i:i+8]))
    ln = result[0] * 256 + result[1]
    exd = len(result) - ln
    return result[exd:]


def get_random_key():
    return random.randrange(0,1<<56,1)
