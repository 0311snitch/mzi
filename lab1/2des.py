import des

if __name__ == '__main__':
    text = bytearray("2DES text", "utf-8")
    print(f"Original text - {text}")
    keys = []
    for _ in range(2):
        key = des.get_random_key()
        keys.append(key)
        text = des.encrypt(text, key)
    keys = keys[::-1]
    print(f"Encrypted text - {text}")
    for i in range(2):
        key = keys[i]
        text = des.decrypt(text, key)
    print(f"Decrypted text - {str(text)}")