import random

FROM = 100
TO = 2000

def isPrime(n):
    if n % 2 == 0:
        return n == 2
    d = 3
    while d * d <= n and n % d != 0:
       d += 2
    return d * d > n

def gcd(a, b):
    result =  a if b == 0 else gcd(b, a % b)
    return result

def get_d(euler, e):
    for i in range(euler):
        d = i * euler + 1
        if d % e == 0:
            return int(d / e)


def modular_exponentation(base, power, m):
    return pow(base, power) % m


def choose_public_exponent(euler):
    while (True):
        e = random.randrange(2, euler)
        if (gcd(e, euler) == 1):
            return e

def chooseKeys():
    rand1, rand2 = 0, 0
    while not isPrime(rand1):
        rand1 = random.randint(FROM, TO)
    while not isPrime(rand2):
        rand2 = random.randint(FROM, TO)
    print(f"{rand1}, {rand2}")
    n = rand1 * rand2
    euler = (rand1 - 1) * (rand2 - 1) 
    e = choose_public_exponent(euler)
    d = get_d(euler, e)    
    return n, e, d

def encrypt_char(char, n_key, e_key):
    return modular_exponentation(ord(char), e_key, n_key)

def decrypt_char(char_code, n_key, d_key):
    return chr(modular_exponentation(char_code, d_key, n_key))


def demo():
    n, e, d = chooseKeys()
    message = input('Enter text to encrypt: ')
    encrypted = []
    for char in message:
        encrypted.append(encrypt_char(char, n, e))
    print('Encrypted text:', encrypted)
    decrypted = []
    for char_code in encrypted:
        decrypted.append(decrypt_char(char_code, n, d))
    print('Decrypted text:',''.join(decrypted))

if __name__ == "__main__":
    demo()