def write(name, keys):
    with open(name, 'w') as f:
        for i in keys:
            f.write(f"{i}\n")


def read(name):
    keys = []
    with open(name, "rb") as f:
        line = f.readline()
        while line:
            keys.append(line)
            line = f.readline()
    return keys


def to_int(raw):
    return int.from_bytes(raw, byteorder="big", signed=False)


def to_bytes(n, size=32):
    return int.to_bytes(n, byteorder="big", length=size, signed=False)


def modular_invert(a, n):
    if a < 0:
        return n - modular_invert(-a, n)
    t, new_t = 0, 1
    r, new_r = n, a
    while new_r != 0:
        quotinent = r // new_r
        t, new_t = new_t, t - quotinent * new_t
        r, new_r = new_r, r - quotinent * new_r
    if r > 1:
        return -1
    if t < 0:
        t += n
    return t