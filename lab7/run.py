from secrets import randbits
from utils import read, write
import sys
from curve import EllipticCurve

common_keys = read(sys.argv[1])

[p, a, b, Gx, Gy, n] = [int(k) for k in common_keys]

d = int(read(sys.argv[2])[0])

curve = EllipticCurve(p, a, b, Gx, Gy)

Q = read(sys.argv[3])

x, _ = curve.exp(d, int(Q[0]), int(Q[1]))

write('secret_key.txt', [x])
print('Users secret key: ', x)