from secrets import randbelow
from utils import read, write
import sys
from curve import EllipticCurve

keys = read(sys.argv[1])

[p, a, b, Gx, Gy, n] = [int(i) for i in keys]

d = randbelow(n)
print(f"d - {d}")
curve = EllipticCurve(p, a, b, Gx, Gy)
Q = curve.exp(d)

write('private_key.bin', [d])
write('public_key.bin', Q)
print('Keys are generated. Private key - private_key.txt. Public key - public_key.txt')